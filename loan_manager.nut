class LoanManager {
	constructor(queueFunc, _services) {
		enqueueAction = queueFunc;
		services = _services;
	}
	
	enqueueAction = false;
	isInitialized = false;
	services = false;

	function Initialize() {
		if (isInitialized)
			return
		isInitialized = true;
		enqueueAction(Update())
		yield "initialized"
	}

	function Update() {
		if (AICompany.GetLoanAmount() == 0)
			return;

		if (AICompany.GetBankBalance(AICompany.COMPANY_SELF) < AICompany.GetLoanInterval())
			return;

		local multiplier = AICompany.GetBankBalance(AICompany.COMPANY_SELF) / AICompany.GetLoanInterval();
		yield "paying off " + multiplier;
		AICompany.SetLoanAmount(AICompany.GetBankBalance(AICompany.COMPANY_SELF) - multiplier * AICompany.GetLoanInterval());
	}

	function Save() {
		return null;
	}
}
