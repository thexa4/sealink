class LakeManager {
	constructor(queueFunc, _services) {
		enqueueAction = queueFunc;
		services = _services;
		lakes = [];
	}
	
	enqueueAction = false;
	isInitialized = false;
	services = false;
	lakes = false;

	function Initialize() {
		if (isInitialized)
			return

		local xsize = AIMap.GetMapSizeX();
		local ysize = AIMap.GetMapSizeY();

		local strips = [];
		local stripLakes = {};
		for(local x=0; x < xsize; x+=1) {
			if (x % 16 == 0)
				yield "Initialization (" + x + " / " + xsize + ")"

			local newStrips = [];
			local currentStrip = false;
			for(local y=0; y < ysize; y += 1) {
				local tile = AIMap.GetTileIndex(x, y);
				local isWater = AITile.HasTransportType(tile, AITile.TRANSPORT_WATER) && AITile.GetMinHeight(tile) == 0;

				if (currentStrip == false && isWater) {
					currentStrip = TileStrip(x, y)
				}

				if (currentStrip != false && !isWater) {
					currentStrip.Close(y, strips);
					if (currentStrip.left.len() == 0) {
						// No neighbors so new lake.
						local lake = Lake();
						stripLakes[currentStrip] <- lake;
						lake.tiles.append(currentStrip);
					}
					if (currentStrip.left.len() == 1) {
						// One neighbor so continue previous lake.
						local lake = stripLakes[currentStrip.left[0]];
						lake.tiles.append(currentStrip)
						stripLakes[currentStrip] <- lake;
					}
					if (currentStrip.left.len() > 1) {
						// Multiple neighbors so merge all of them.
						local lake = Lake();
						local tiles = {};

						foreach (neighbor in currentStrip.left) {
							foreach (tile in stripLakes[neighbor].tiles) {
								tiles[tile] <- true;
							}
						}
						foreach (tile, _ in tiles) {
							lake.tiles.append(tile);
							stripLakes[tile] = lake;
						}
						stripLakes[currentStrip] <- lake;
						lake.tiles.append(currentStrip)
					}

					newStrips.append(currentStrip);
					currentStrip = false;
				}
			}
			strips = newStrips;
		}

		local lakeAggregate = {};
		foreach (lake in stripLakes) {
			lakeAggregate[lake] <- true;
		}
		local counter = 0;
		foreach (lake, _ in lakeAggregate) {
			lakes.append(lake);
			counter += 1;

			lake.UpdateStats();

			yield "Found lake " + counter + ": " + lake.ToString();
			enqueueAction(lake.UpdateTradeOpportunities());

			if (AIController.GetSetting("debug_lake_signs") == 1) {
				local tile = lake.tiles[lake.tiles.len() / 2];
				local y = (tile.y_end - tile.y_start) / 2 + tile.y_start;
				AISign.BuildSign(AIMap.GetTileIndex(tile.x, y), "Lake " + counter);
			}
		}

		isInitialized = true;
		yield "initialized"

	}

	function Update() {
		return; //TODO: add updating lakes
	}

	function Save() {
		return null;
	}

}

class Lake {
	constructor() {
		tiles = [];
	}

	tiles = false;
	coast = false;
	area = -1;
	x_min = -1;
	x_max = -1;
	y_min = -1;
	y_max = -1;

	tradeOpportunities = false;

	function UpdateStats() {
		area = 0;
		x_max = 0;
		y_max = 0;
		x_min = AIMap.GetMapSizeX() - 1;
		y_min = AIMap.GetMapSizeY() - 1;

		coast = AITileList();

		foreach (tile in tiles) {
			area += tile.Length();
			x_min = min(x_min, tile.x);
			x_max = max(x_max, tile.x);
			y_min = min(y_min, tile.y_start);
			y_max = max(y_max, tile.y_end);

			coast.AddRectangle(AIMap.GetTileIndex(tile.x - 1, tile.y_start - 1), AIMap.GetTileIndex(tile.x + 1, tile.y_end + 1));
		}
		coast.Valuate(AITile.IsCoastTile);
		coast.KeepValue(1);
	}

	function GetPotentialDockLocations() {
		local result = AITileList();

		foreach (tile, _ in coast) {
			local slope = AITile.GetSlope(tile);
			local x = AIMap.GetTileX(tile);
			local y = AIMap.GetTileY(tile);
			local isSlopeOk = false;
			local tilePos = -1;

			switch (slope) {
				case AITile.SLOPE_NW:
					isSlopeOk = true;
					tilePos = AIMap.GetTileIndex(x, y + 1);
					break;
				case AITile.SLOPE_SW:
					isSlopeOk = true;
					tilePos = AIMap.GetTileIndex(x - 1, y);
					break;
				case AITile.SLOPE_SE:
					isSlopeOk = true;
					tilePos = AIMap.GetTileIndex(x, y - 1);
					break;
				case AITile.SLOPE_NE:
					tilePos = AIMap.GetTileIndex(x + 1, y);
					isSlopeOk = true;
					break;
			}
			if (!isSlopeOk)
				continue;
			if (!AITile.IsWaterTile(tilePos))
				continue;

			result.AddTile(tile);
		}

		result.Valuate(AITile.IsBuildable);
		result.KeepValue(1);

		return result;
	}

	function UpdateTradeOpportunities() {
		local dockLocations = GetPotentialDockLocations();
		local dockRadius = AIStation.GetCoverageRadius(AIStation.STATION_DOCK);

		local buyOpportunities = {};
		local sellOpportunities = {};

		tradeOpportunities = {
			buy = buyOpportunities
			sell = sellOpportunities
		};

		local cargoList = AICargoList();
		foreach (cargo, _ in cargoList) {
			yield "Updating cargo " + AICargo.GetCargoLabel(cargo) + " #" + cargo
			local sellTiles = {};
			local buyTiles = {};
			buyOpportunities[cargo] <- buyTiles;
			sellOpportunities[cargo] <- sellTiles;

			local cargoProducers = AITileList();
			local cargoConsumers = AITileList();
			cargoProducers.AddList(dockLocations);
			cargoConsumers.AddList(dockLocations);

			// maybe add radius expansion via nonfunctional loading dock later?

			cargoProducers.Valuate(AITile.GetCargoProduction, cargo, 1, 1, dockRadius);
			cargoConsumers.Valuate(AITile.GetCargoAcceptance, cargo, 1, 1, dockRadius);
			cargoProducers.RemoveValue(0);
			cargoConsumers.RemoveValue(0);

			foreach (tile, amount in cargoProducers) {
				buyOpportunities[tile] <- amount;
			}
			foreach (tile, amount in cargoConsumers) {
				sellOpportunities[tile] <- amount;
			}
		}
	}

	function ToString() {
		return "Lake (x = [" + x_min + ", " + x_max + "], y = [" + y_min + ", " + y_max + "], area = " + area + ", coast size = " + coast.Count() + ")";
	}
}

class TileStrip {
	constructor(_x, _y_start) {
		x = _x;
		y_start = _y_start;
		left = [];
		right = [];
	}

	x = -1;
	y_start = -1; //inclusive start position
	y_end = -1;   //inclusive end position
	left = false;
	right = false;

	function Length() {
		return y_end - y_start + 1;
	}

	function IsLeftNeighbor(other) {
		if (other.x + 1 != x)
			return false;
		if (other.y_end < y_start)
			return false;
		if (other.y_start > y_end)
			return false;
		return true;
	}

	function Close(y, possible_neighbors) {
		y_end = y - 1;

		foreach (neighbor in possible_neighbors) {
			if (IsLeftNeighbor(neighbor)) {
				left.append(neighbor);
				neighbor.right.append(this);
			}
		}
	}

	function ToString() {
		return "TileStrip (x = " + x + ", y = [" + y_start + ", " + y_end + "])"
	}
}
