class SealinkAIInfo extends AIInfo {
	function GetAuthor() 		{ return "Max Maton"; }
	function GetName() 		{ return "Sealink"; }
	function GetShortName() 	{ return "SEAL"; }
	function GetDescription() 	{ return "Boat AI"; }
	function GetVersion() 		{ return 1; }
	function MinVersionToLoad() 	{ return 1; }
	function GetDate() 		{ return "2019-08-28"; }
	function UseAsRandomAI() 	{ return true; }
	function CreateInstance() 	{ return "SealinkAI"; }
	function GetAPIVersion() 	{ return "1.9"; }
	function GetURL() 		{ return "https://gitlab.com/thexa4/sealink/"; }

	function GetSettings() {
                AddSetting({name = "log_level", description = "Debug: Log level (higher = print more)", easy_value = 3, medium_value = 3, hard_value = 3, custom_value = 3, flags = CONFIG_INGAME, min_value = 1, max_value = 3});
                AddLabels("log_level", {_1 = "1: Info", _2 = "2: Verbose", _3 = "3: Debug" } );
                AddSetting({name = "debug_lake_signs", description = "Debug: Place signs with internal ids of the lakes.", easy_value = 0, medium_value = 0, hard_value = 0, custom_value = 0, flags = 0, min_value = 0, max_value = 1});
        }
}

RegisterAI(SealinkAIInfo());
