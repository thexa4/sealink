class BusinessPlanManager {
	constructor(queueFunc, _services) {
		enqueueAction = queueFunc;
		services = _services;
		plansQueue = [];
	}
	
	enqueueAction = false;
	isInitialized = false;
	services = false;
	plansQueue = false;
	plans = {};

	function Initialize() {
		if (isInitialized)
			return
		isInitialized = true;
		yield "initialized"
	}

	function SubmitPlan(plan) {
		plansQueue.push(plan);
	}

	function Update() {
		local prevQueue = plansQueue;
		plansQueue = [];

		foreach (plan in prevQueue) {
			local testMode = AITestMode();
			local accountant = AIAccounting();
			plan.Run();
			plan.cost = accountant.GetCosts() + plan.unaccountableCosts;
			yield "Evaluated " + plan.label + ": " + plan.cost
		}
	}

	function Save() {
		return null;
	}
}

class BusinessPlan {
	constructor(label, func, userdata) {
		this.label = label;
		this.func = func;
		this.userdata = userdata;
	}

	label = "unknown"
	cost = -1;
	unaccountableCosts = -1;
	evaluated = false;
	func = null;
	userdata = null;

	function Run() {
		userdata.customCosts <- 0
		func.bindenv(userdata)()
		unaccountableCosts = customCosts;
	}
}
