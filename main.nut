require("loan_manager.nut")
require("lake_manager.nut")
require("business_plan_manager.nut")

::globals <- this;
class SealinkAI extends AIController {
	workQueue = [];
	workQueueLabels = [];
	services = {};
	serviceClosures = [];
	serviceList = [
		"LoanManager",
		"LakeManager",
		"BusinessPlanManager",
	];

	constructor() {

	}

	function Start();
	function FindSomethingToDo()

	function QueueAction(func);
	function QueueActionClosure(func) {
		ai.QueueAction.call(ai, label, func)
	}

	function Save() {
		local serviceData = {};
		foreach (name, service in services) {
			serviceData[name] <- service.Save();
		}
		return {
			services = serviceData,
		}
	}
}

function SealinkAI::QueueAction(label, func) {
	workQueue.push(func);
	workQueueLabels.push(label);
}

function SealinkAI::Start() {
	foreach (name in serviceList) {
		local obj = {
			ai = this,
			label = name,
		}
		local service = ::globals[name]
		serviceClosures.push(obj);
		services[name] <- service(QueueActionClosure.bindenv(obj), services);
		
	}

	foreach (name, service in services) {
		QueueAction(name, service.Initialize());
	}

	while(true) {

		if (workQueue.len() == 0)
			FindSomethingToDo();

		local currentList = workQueue;
		local currentLabels = workQueueLabels;
		workQueue = [];
		workQueueLabels = [];

		foreach(i, workItem in currentList) {
			if (workItem == null || workItem.getstatus() == "dead")
				continue;

			local output = resume workItem;
			local label = currentLabels[i];
			if (workItem.getstatus() != "dead") {
				workQueue.push(workItem);
				workQueueLabels.push(label);
			}
			if (output != null) {
				local tickPadding = "______"
				local opsPadding = "____"
				local tickString = "" + GetTick();
				local opsString = "" + GetOpsTillSuspend();
				local tickDisplay = tickPadding.slice(tickString.len()) + tickString;
				local opsDisplay = opsPadding.slice(opsString.len()) + opsString;
				AILog.Info(tickDisplay + " " + opsDisplay + " " + label + "# " + output)
			}
		}
		
		if (workQueue.len() == 0)
			this.Sleep(10 - (this.GetTick() % 10));
	}
}

function SealinkAI::FindSomethingToDo() {
	foreach(name, service in services) {
		QueueAction(name, service.Update());
	}
}
